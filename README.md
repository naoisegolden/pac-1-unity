# PAC1 - Un joc d'aventures

Author: Naoise Golden Santos <naoise@uoc.edu>

Gameplay video: https://youtu.be/JIwQQJX2o84

## Solution description

### First screen: Main Menu

In this scene there are only 2 buttons: Start Game and Exit (pretty self explanatory).

It has a soundtrack that plays on loop while in the screen and some art.

### Second screen: The Game

This scene is the main game. The elements in this scene are:

* Player 1 (Guybrush)'s dialogue text
* Player 2 (Carla)'s dialogue text
* Text to announce the winner of the round
* UI to advance to the next round and an option to facilitate Carla winning (for debugging testing purposes)
* A scrollable list of text dynamically generated to display the available dialogue options
* Background art

It also has a soundtrack that plays on loop while in the screen.

Data is loaded from a JSON file in the Resources folder.

The main game mechanic is based on a `Round`, which consists of the following steps:

* Start
* List Challenges
* Pick Challenge
* Show Challenge
* List Responses
* Pick Response
* Show Response
* Fight
* End

Depending on who's turn is it (`IsPlayerTurn` true or false), each step has 2 different actions. If `IsPlayerTurn` is `true`:

* A list of challenges (insults) is displayed.
* The player picks a "challenge".
* The challenge is displayed.
* Carla picks and displays the response.

If `IsPlayerTurn` is `false`:

* Carla picks and displays a challenge.
* A list of responses is displayed.
* The player picks a response.
* The response is displayed a response.

After this, based on the challenge and response list indexes, the script determines who won this round. If the response is the same index as the challenge, whoever gave the response, wins. If the player won:

* Guybrush is animated with the attack animation.
* Carla is animated with the defense animation.

If Carla won: viceversa. In all cases, a text is displayed indicating who won, and the win count UI is updated.

At the end of the round, the script checks if anyone won `MaxWins`, in which case there is an animation and the game ends, opening the End scene. If nobody won `MaxWins` it starts over again with a new `Round`.

Carla has two modes: when "Carla wins next" is on, the next response will always be correct. When it isn't on, the response is random, using `UnityEngine.Random`.

### Last screen: The End

This scene simply displays a different text depending on the value of the winner taken from `PlayerPrefs`, and displays a button with an attached script that navigates to the Main Menu.

## How to play

Download the project and open it with Unity Editor version 2020.3.18f1. Open the `MainMenu` scene and run the game (play).

Click "Start Game". The first round you (Guybrush) starts by selecting an insult, and Carla chooses a random response. If she chooses the right response, she wins. If not, you win. Whoever wins, starts the next round.

Click the "Next round" button. If you wish for Carla to win the next round, toggle "Carla wins next" before clicking the button.

If you won the round, you will be presented again with the available insults. If Carla won, she will display a random insult and you will be presented with the available responses. You win by selecting the right response for that insult. If you select the right response, you win; if not, Carla wins.

And so on.

Once anybody has won 3 rounds, the game ends. Click "Back to start" to start over again.

## Implementation details

I created a prefab button so that all buttons in the game have the same base style. In this scene there are only 2 buttons: Start Game and Exit.

The buttons in the `MainMenu` and `End` scenes have their own script components attached and self-referenced in the On Click event. The Exit button calls a function that exits the game with `Application.Quit()`, and the Start Game button calls a function that opens the Game scene with `SceneManager.LoadScene()`.

The `Game` scene encapsulates all the functionality in one single script attached to an empty `GameObject` called GameplayManager. I preferred to put it all in a single script to make it simpler, instead of creating separate scripts for each player or for the UI, since this game is not meant to scale or add new players or functionalities.

The game script gets all the scene objects and information via `[SerializeField] private` attributes. All the interactive elements and UI components need to be passed to the script. The script allows to configure the `MaxWins`, so it's a dynamic value. 

The players don't have their owns script, but there is a custom `Player` class in the main script to encapsulate animation functionalities, and player information such as name or number of wins.

There are state machine for the 2 characters for the following animations: idle, talking, en guard, en guard talking, unsheathe, attack, defend and loose.

Because of all the animations, almost all steps in the `Round` are coroutines. Most animations wait for `const WaitInterval` seconds before announcing the next step is ready. The `Round` function uses `WaitUntil` and a custom `enum RoundStep` to know when the steps are ready.

The information of the winner is sent to the end scene using `PlayerPrefs`.

The whole game UI is based on a Canvas, since there is no world to interact. The text options are implemented in a Scroll View. 

The aspect ratio of the viewport and the build are based on a handheld in portrait mode. The only build configured is for iPhone, and it's been tested on an iPhone 7.

In general, the code follows the ["single-responsibility principle"](https://en.wikipedia.org/wiki/Single-responsibility_principle) and the ["interface segregation principle"](https://en.wikipedia.org/wiki/Interface_segregation_principle), both part of the SOLID principles. It also attempts to follow the "separation of concerns" design principle for the data logic, game logic and UI, but it's difficult with Unity's engine and using a single script.

## Resources

* Guybrush and Carla sprites: https://amiga.cafe/mi/
* Intro and game music: https://scummbar.com/resources/downloads/index.php?todo=MP3
* Sword fight sound effect: http://freesoundeffect.net/sound/sword-fight-sound-effect
* Carla's House background image: https://www.spriters-resource.com/pc_computer/secretofmonkeyisland/
* Title image: https://archive.org/details/monkey-island_202107
* Insults: http://gamelosofy.com/los-insultos-de-el-secreto-de-monkey-island-1/
* Font: https://www.dafont.com/press-start.font