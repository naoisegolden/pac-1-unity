using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player
{
	public int wins = 0;
	public string name;
	private Text textObject;
	private Animator animator;

	public Player(GameObject ga, string name, Text textObject)
	{
		animator = ga.gameObject.GetComponent<Animator>();
		this.name = name;
		this.textObject = textObject;
	}

	public void Speak(string message)
	{
		animator.SetBool("IsSpeaking", true);
		textObject.text = message;
	}

	public void StopSpeaking()
	{
		animator.SetBool("IsSpeaking", false);
	}

	public void Unsheathe()
	{
		animator.SetTrigger("Unsheathe");
	}
	public void EnGuard()
	{
		animator.SetTrigger("EnGuard");
	}

	public void Attack()
	{
		animator.SetTrigger("Attack");
	}

	public void Defend()
	{
		animator.SetTrigger("Defend");
	}

	public void Lose()
	{
		animator.SetTrigger("Lose");
	}

	public override string ToString()
	{
		return name;
	}
}
public class GameData
{
	public string[] challenges;
	public string[] responses;
}

enum RoundStep
{
	Start,
	ListAttacks,
	PickAttack,
	ShowAttack,
	ListResponses,
	PickResponse,
	ShowResponse,
	Fight,
	End,
}

public class Gameplay : MonoBehaviour
{
	[SerializeField] private Text GuybrushText;
	[SerializeField] private Text CarlaText;
	[SerializeField] private Text ResultText;
	[SerializeField] private Text WinsCountTextGuybrush;
	[SerializeField] private Text WinsCountTextCarla;
	[SerializeField] private Button NextRoundButton;
	[SerializeField] private Toggle CarlaAlwaysWinsToggle;
	[SerializeField] private Transform TextOptionsContainer;
	[SerializeField] private GameObject TextOptionPrefab;
	[SerializeField] private GameObject GuybrushObject;
	[SerializeField] private GameObject CarlaObject;
	[SerializeField] private AudioSource FightSoundEffect;
	[SerializeField] private int MaxWins = 3;

	private string[] Challenges = new string[] { };
	private string[] Responses = new string[] { };
	private int ChallengeIndex;
	private int ResponseIndex;
	private bool IsPlayerTurn;
	private bool HasUserPicked = false;
	private RoundStep RoundNextStep;
	private Player Guybrush;
	private Player Carla;
	private const float WaitInterval = 3.0f;

	void Start()
	{
		NextRoundButton.onClick.AddListener(OnClickNewRound);

		PlayerPrefs.DeleteKey("Winner");

		Guybrush = new Player(GuybrushObject, "Guybrush", GuybrushText);
		Carla = new Player(CarlaObject, "Carla", CarlaText);

		LoadGameData();

		// Random player turn
		IsPlayerTurn = UnityEngine.Random.value > 0.5f;

		StartCoroutine(Intro());
	}

	private IEnumerator Intro()
	{
		CleanTextDialogues();
		HideUI();

		Guybrush.Speak("My name is Guybrush Threepwood, I've come to kill you.");
		yield return new WaitForSeconds(WaitInterval);
		Guybrush.StopSpeaking();
		CleanTextDialogues();

		Carla.Speak("Let's get this over with.");
		yield return new WaitForSeconds(WaitInterval);
		Carla.StopSpeaking();
		CleanTextDialogues();

		Guybrush.Unsheathe();
		Carla.Unsheathe();
		yield return new WaitForSeconds(WaitInterval);

		// Start Round
		StartCoroutine(Round());
	}

	/* Round order is
	 * - RoundStart
	 * - RoundListAttacks
	 * - RoundPickAttack
	 * - RoundShowAttack
	 * - RoundListResponses
	 * - RoundPickResponse
	 * - RoundShowResponse
	 * - RoundFight
	 * - RoundEnd
	 */
	private IEnumerator Round()
	{
		// Start 
		RoundStart();

		// List attacks
		yield return new WaitUntil(() => RoundNextStep == RoundStep.ListAttacks);
		RoundListAttacks();

		// Pick attack
		yield return new WaitUntil(() => RoundNextStep == RoundStep.PickAttack);
		StartCoroutine(RoundPickAttack());

		// Show attack
		yield return new WaitUntil(() => RoundNextStep == RoundStep.ShowAttack);
		StartCoroutine(RoundShowAttack());

		// List responses
		yield return new WaitUntil(() => RoundNextStep == RoundStep.ListResponses);
		RoundListResponses();

		// Pick response
		yield return new WaitUntil(() => RoundNextStep == RoundStep.PickResponse);
		StartCoroutine(RoundPickResponse());

		// Show response
		yield return new WaitUntil(() => RoundNextStep == RoundStep.ShowResponse);
		StartCoroutine(RoundShowResponse());

		// Fight scene
		yield return new WaitUntil(() => RoundNextStep == RoundStep.Fight);
		StartCoroutine(RoundFight());

		// End
		yield return new WaitUntil(() => RoundNextStep == RoundStep.End);
		StartCoroutine(RoundEnd());
	}

	private void RoundStart()
	{
		CleanTextDialogues();
		CleanTextOptions();
		HideUI();

		RoundNextStep = RoundStep.ListAttacks;
	}
	private void RoundListAttacks()
	{
		if (IsPlayerTurn)
		{
			// List attacks
			ListChallenges();
		}
		else
		{
			// Do nothing
		}

		RoundNextStep = RoundStep.PickAttack;
	}

	private IEnumerator RoundPickAttack()
	{
		if (IsPlayerTurn)
		{
			// Do nothing (wait for user interaction)
			yield return new WaitUntil(() => HasUserPicked);
			HasUserPicked = false;
			CleanTextOptions();
		}
		else
		{
			// Carla picks random challenge
			NewCarlaChallenge();
			yield return null;
		}

		RoundNextStep = RoundStep.ShowAttack;
	}

	private IEnumerator RoundShowAttack()
	{
		if (IsPlayerTurn)
		{
			// Show attack text in Guybrush text
			// Player animation talk
			Guybrush.Speak(Challenges[ChallengeIndex]);
		}
		else
		{
			// Show attack text in Carla text
			// Carla animation talk
			Carla.Speak(Challenges[ChallengeIndex]);
		}

		// Wait N seconds
		yield return new WaitForSeconds(WaitInterval);
		// Reset
		Guybrush.StopSpeaking();
		Carla.StopSpeaking();
		if (IsPlayerTurn) CleanTextDialogues();

		RoundNextStep = RoundStep.ListResponses;
	}

	private void RoundListResponses()
	{
		if (IsPlayerTurn)
		{
			// Do nothing
		}
		else
		{
			// List responses
			ListResponses();
		}

		RoundNextStep = RoundStep.PickResponse;
	}

	private IEnumerator RoundPickResponse()
	{
		if (IsPlayerTurn)
		{
			// Carla picks random response (based on difficulty)
			NewCarlaResponse();
			yield return null;
		}
		else
		{
			// Do nothing (wait for user interaction)
			yield return new WaitUntil(() => HasUserPicked);
			HasUserPicked = false;
			CleanTextDialogues();
			CleanTextOptions();
		}

		RoundNextStep = RoundStep.ShowResponse;
	}

	private IEnumerator RoundShowResponse()
	{
		if (IsPlayerTurn)
		{
			// Show response text in Carla text
			// Carla animation talk
			Carla.Speak(Responses[ResponseIndex]);
		}
		else
		{
			// Show response text in Guybrush text
			// Carla animation talk
			Guybrush.Speak(Responses[ResponseIndex]);
		}

		// Wait N seconds
		yield return new WaitForSeconds(WaitInterval);
		// Reset
		CleanTextDialogues();
		Guybrush.StopSpeaking();
		Carla.StopSpeaking();

		RoundNextStep = RoundStep.Fight;
	}

	private IEnumerator RoundFight()
	{
		// Sound effect
		FightSoundEffect.Play();

		if (GetWinner() == Guybrush)
		{
			Guybrush.Attack();
			Carla.Defend();
		}
		else
		{
			Guybrush.Defend();
			Carla.Attack();
		}

		// Wait N seconds
		yield return new WaitForSeconds(WaitInterval);
		// Reset
		Guybrush.EnGuard();
		Carla.EnGuard();
		FightSoundEffect.Stop();

		RoundNextStep = RoundStep.End;
	}

	private IEnumerator RoundEnd()
	{
		// Show result
		UpdateWins();
		UpdateWinsCount();
		ResultText.text = GetResultText();

		// If end game, animate and load new scene
		yield return StartCoroutine(CheckEndGame());

		// If not, wait N seconds before ending round
		yield return new WaitForSeconds(WaitInterval);

		// Assign next turn
		IsPlayerTurn = GetWinner() == Guybrush;
		// Display UI
		CleanTextDialogues();
		RestartUI();
		ShowUI();

		RoundNextStep = RoundStep.Start;
	}

	private void ShowUI()
	{
		NextRoundButton.gameObject.SetActive(true);
		if (IsPlayerTurn) CarlaAlwaysWinsToggle.gameObject.SetActive(true);
	}

	private void HideUI()
	{
		NextRoundButton.gameObject.SetActive(false);
		CarlaAlwaysWinsToggle.gameObject.SetActive(false);
	}

	private void RestartUI()
	{
		CarlaAlwaysWinsToggle.isOn = false;
	}

	private void UpdateWinsCount()
	{
		WinsCountTextGuybrush.text = $"{Guybrush.wins}/{MaxWins}";
		WinsCountTextCarla.text = $"{Carla.wins}/{MaxWins}";
	}

	private void NewCarlaChallenge()
	{
		ChallengeIndex = Random.Range(0, Challenges.Length);
	}

	private void NewCarlaResponse()
	{
		if (CarlaAlwaysWinsToggle.isOn)
		{
			ResponseIndex = ChallengeIndex;
		}
		else
		{
			ResponseIndex = Random.Range(0, Responses.Length);
		}
	}

	private void OnClickTextOption(int index)
	{
		if (IsPlayerTurn)
		{
			// Pick a challenge
			OnClickChallenge(index);
		}
		else
		{
			// Pick a response
			OnClickResponse(index);
		}

		HasUserPicked = true;
	}

	private void OnClickChallenge(int index)
	{
		ChallengeIndex = index;
	}

	private void OnClickResponse(int index)
	{
		ResponseIndex = index;
	}

	private Player GetWinner()
	{
		if (IsPlayerTurn && ChallengeIndex != ResponseIndex || !IsPlayerTurn && ChallengeIndex == ResponseIndex)
		{
			return Guybrush;
		}

		if (IsPlayerTurn && ChallengeIndex == ResponseIndex || !IsPlayerTurn && ChallengeIndex != ResponseIndex)
		{
			return Carla;
		}

		return null;
	}

	private void UpdateWins()
	{
		Player winner = GetWinner();

		if (winner == Guybrush)
		{
			Guybrush.wins += 1;
		}
		else
		{
			Carla.wins += 1;
		}
	}

	private IEnumerator CheckEndGame()
	{
		Player winner = GetWinner();


		if (Carla.wins == MaxWins)
		{
			Guybrush.Lose();
			yield return new WaitForSeconds(WaitInterval);
			CleanTextDialogues();
			Carla.Speak("You lose! I told you you didn't stand a chance.");
		}
		if (Guybrush.wins == MaxWins)
		{
			Carla.Lose();
			yield return new WaitForSeconds(WaitInterval);
			CleanTextDialogues();
			Carla.Speak("OK, you win. I hope you're happy.");
		}

		if (Carla.wins == MaxWins || Guybrush.wins == MaxWins)
		{
			// Wait N seconds for animations to end
			yield return new WaitForSeconds(WaitInterval);
			CleanTextDialogues();
			Carla.StopSpeaking();
			Guybrush.StopSpeaking();

			// Wait N seconds before end scene
			yield return new WaitForSeconds(WaitInterval);

			// Persist winner and load end scene
			PlayerPrefs.SetString("Winner", winner.name);
			SceneManager.LoadScene("TheEnd");
		}
	}

	private string GetResultText()
	{
		if (GetWinner() == Guybrush)
		{
			return "You won this round!";
		}

		if (GetWinner() == Carla)
		{
			return $"{Carla.name} won this round!";
		}

		return null;
	}

	private void OnClickNewRound()
	{
		StartCoroutine(Round());
	}

	private void LoadGameData()
	{
		TextAsset file = Resources.Load<TextAsset>("GameData");
		GameData data = JsonUtility.FromJson<GameData>(file.text);
		Challenges = data.challenges;
		Responses = data.responses;
	}

	private void PopulateTextOptions(string[] options)
	{
		var index = 0;
		foreach (var option in options)
		{
			var textOptionObject = Instantiate(TextOptionPrefab, TextOptionsContainer, false);
			textOptionObject.GetComponent<Text>().text = option;
			var textOptionIndex = index;
			textOptionObject.GetComponent<Button>().onClick.AddListener(() => OnClickTextOption(textOptionIndex));

			index++;
		}
	}

	private void CleanTextOptions()
	{
		foreach (Transform child in TextOptionsContainer.transform)
		{
			Destroy(child.gameObject);
		}
	}

	private void CleanTextDialogues()
	{
		CarlaText.text = null;
		GuybrushText.text = null;
		ResultText.text = null;
	}

	private void ListChallenges()
	{
		PopulateTextOptions(Challenges);
	}

	private void ListResponses()
	{
		PopulateTextOptions(Responses);
	}
}
