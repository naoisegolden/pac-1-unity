using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TheEndResult : MonoBehaviour
{
	[SerializeField] private Text ResultText;
	private const string Guybrush = "Guybrush";
	private const string Carla = "Carla";

	void Start()
	{
		if (!PlayerPrefs.HasKey("Winner")) return;

		string winner = PlayerPrefs.GetString("Winner");

		if (winner == Guybrush)
		{
			ResultText.text = "Congrats! You have won the game.";
		}

		if (winner == Carla)
		{
			ResultText.text = "Carla has won the game. Better luck next time.";
		}
	}
}
